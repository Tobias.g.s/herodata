﻿using DataAccess.Models;
using DataAccess.Repositories;
using System.Security.Cryptography.X509Certificates;

internal class Program
{
    private static void Main(string[] args)
    {
        // Get and write out all customers
        Console.WriteLine("Get and write out all customers \n");
        CustomerRepository _repo = new CustomerRepository();
        List<Customer> customers = _repo.GetAll().ToList<Customer>();

        foreach (Customer customer in customers)
        {
            Console.WriteLine(customer);
        }

        // get and write out subset of customers
        Console.WriteLine(" \n Get and write out subset of customers \n");
        List<Customer> DogShitcustomers = _repo.GetSubset(5, 1).ToList<Customer>();
        foreach (Customer customer in DogShitcustomers)
        {
            Console.WriteLine(customer);

        }
        // get and write spesific customer
        Console.WriteLine("\n Get and write out spesific customers \n");
        Console.WriteLine(_repo.Get("Lui"));
        // get and write out customer count in all contries
        Console.WriteLine("\n get and write out customer count in all contries \n");
        List<CustomerCountry> customerCountries = _repo.GetCustomersInCountries().ToList<CustomerCountry>();
        foreach (CustomerCountry cc in customerCountries)
        {
            Console.WriteLine(cc.Country + " : " + cc.Customers);
        }
        // update customer

        Console.WriteLine("\n  update customer \n");
        Customer Knut = _repo.Get("Knut") ?? new Customer();
        Knut.LastName = "Dong";
        Knut.Country = "Sweden";
        Knut.Email = "Knut@Dong.sv";
        Console.WriteLine(_repo.Get("Knut"));
        Console.WriteLine( _repo.Update(Knut));
        Console.WriteLine(_repo.Get("Knut"));

        // get list of customers ordered by total spent.
        Console.WriteLine("\n get list of customers ordered by total spent. \n ");
        List<CustomerSpender> spenders = _repo.GetHighestSpender().ToList<CustomerSpender>();
        foreach (CustomerSpender spender in spenders)
        {
            Console.WriteLine(spender);
        }
        // get most popular genre for spesific user
        Console.WriteLine("\n get most popular genre for spesific user \n");
        foreach (var genre in _repo.GetMostPopularGenre(1))
        {
            Console.WriteLine(genre.genre + " " + genre.Count);
        }
    }

}