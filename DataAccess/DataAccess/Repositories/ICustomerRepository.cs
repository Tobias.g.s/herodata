﻿using DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public interface ICustomerRepository
    {
        ICollection<Customer> GetAll(); // 1
        ICollection<Customer> GetSubset(int limit, int offset); // 4
        ICollection<CustomerCountry> GetCustomersInCountries(); // 7
        ICollection<CustomerSpender> GetHighestSpender(); // 8 // desc
        ICollection<CustomerGenre> GetMostPopularGenre(int id); // 9
        Customer? Get(int id);  // 2
        Customer? Get(string name); // 3
        bool Add(Customer customer); // 5
        bool Update(Customer customer); // 6
    }
}
