﻿using DataAccess.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Reflection.PortableExecutable;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        /// <summary>
        /// Creates the connection string.
        /// </summary>
        /// <returns></returns>
        public string GetConnectionString()
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = "N-NO-01-01-8236\\SQLEXPRESS";
            builder.InitialCatalog = "Chinook";
            builder.TrustServerCertificate = true;
            builder.IntegratedSecurity= true;
            return builder.ConnectionString; 
        }
        

        public bool Add(Customer customer)
        {
            bool sucsess = false;
            try
            {
                using SqlConnection con = new SqlConnection(GetConnectionString());
                con.Open();
                string sql = "INSERT INTO Customer (FirstName, LastName, Country, PostalCode, Phone, Email) VALUES(@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";
                string FirstName = customer.FirstName;
                string LastName = customer.LastName;
                string Country = customer.Country;
                string? PostalCode = customer.PostalCode;
                string? Phone = customer.PhoneNumber;
                string? Email = customer.Email;
                using (SqlCommand command = new SqlCommand(sql, con))
                {
                    command.Parameters.AddWithValue("@FirstName", FirstName);
                    command.Parameters.AddWithValue("@LastName", LastName);
                    command.Parameters.AddWithValue("@Country", Country);
                    command.Parameters.AddWithValue("@PostalCode", PostalCode);
                    command.Parameters.AddWithValue("@Phone", Phone);
                    command.Parameters.AddWithValue("@Email", Email);
                    sucsess = command.ExecuteNonQuery() == 1 ? true : false;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return sucsess;
        }

        /// <summary>
        /// Gets Customer based on id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>return null on no customer found.</returns>
        public Customer? Get(int id)
        {
            Customer customer;
            try
            {
                using SqlConnection con = new SqlConnection(GetConnectionString());
                con.Open();
                string sql = $"SELECT * from Customer Where CustomerId = {id}";
                using SqlCommand command = new SqlCommand(sql, con);
                using SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    customer = new Customer()
                    {
                        Id = reader.GetInt32(0), // id
                        FirstName = reader.GetString(1), // fname
                        LastName = reader.GetString(2), // lname
                        Country = reader.GetString(7), // country
                        PostalCode = reader.IsDBNull(8) ? null : reader.GetString(8), // postcode
                        PhoneNumber = reader.IsDBNull(9) ? null : reader.GetString(9), // phone
                        Email = reader.GetString(11)// email
                    };
                    return customer;
                }
            }
            catch (Exception)
            {

                throw;
            }
            return null;
        }
        /// <summary>
        /// Gets customer with given firstname, if there is no exact match it will find the first partial match.
        /// </summary>
        /// <param name="name"> complete firstname or parital.</param>
        /// <returns>Customer object</returns>
        public Customer? Get(string name)
        {
            try
            {
                Customer customer;
                using SqlConnection con = new SqlConnection(GetConnectionString());
                con.Open();
                string sql = $"select * from CUstomer where Firstname = '{name}'";
                using SqlCommand command = new SqlCommand(sql, con);
                using SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    if (reader.HasRows)
                    {
                        customer = new Customer()
                        {
                            Id = reader.GetInt32(0), // id
                            FirstName = reader.GetString(1), // fname
                            LastName = reader.GetString(2), // lname
                            Country = reader.GetString(7), // country
                            PostalCode = reader.IsDBNull(8) ? null : reader.GetString(8), // postcode
                            PhoneNumber = reader.IsDBNull(9) ? null : reader.GetString(9), // phone
                            Email = reader.GetString(11)// email
                        };
                        return customer;
                    }
                    else
                    {
                        break;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            try
            {
                Customer customer;
                using SqlConnection con = new SqlConnection(GetConnectionString());
                con.Open();
                string sql = $"select * from Customer where Firstname like '%{name}%'";
                using SqlCommand command = new SqlCommand(sql, con);
                using SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    if (reader.HasRows)
                    {
                        customer = new Customer()
                        {
                            Id = reader.GetInt32(0), // id
                            FirstName = reader.GetString(1), // fname
                            LastName = reader.GetString(2), // lname
                            Country = reader.GetString(7), // country
                            PostalCode = reader.IsDBNull(8) ? null : reader.GetString(8), // postcode
                            PhoneNumber = reader.IsDBNull(9) ? null : reader.GetString(9), // phone
                            Email = reader.GetString(11)// email
                        };
                        return customer;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return null;
        }

        public ICollection<Customer> GetAll()
        {
            ICollection<Customer> customers = new List<Customer>(); ;
            try
            {
                using SqlConnection con = new SqlConnection(GetConnectionString());
                con.Open();
                string sql = "SELECT * FROM CUSTOMER";
                using SqlCommand command = new SqlCommand(sql, con);
                using SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    customers.Add(new Customer()
                    {
                        Id = reader.GetInt32(0), // id
                        FirstName = reader.GetString(1), // fname
                        LastName = reader.GetString(2), // lname
                        Country = reader.GetString(7), // country
                        PostalCode = reader.IsDBNull(8) ? null : reader.GetString(8), // postcode
                        PhoneNumber = reader.IsDBNull(9) ? null : reader.GetString(9), // phone
                        Email = reader.GetString(11)// email
                    });
                }
            }
            catch (Exception)
            {
                throw;
            }
            return customers;
        }

        /// <summary>
        /// Gets all countires and their customer count in descending order.
        /// </summary>
        /// <returns>CustomerCountry List</returns>
        public ICollection<CustomerCountry> GetCustomersInCountries()
        {
            try
            {
                ICollection<CustomerCountry> customers = new List<CustomerCountry>(); ;
                using SqlConnection con = new SqlConnection(GetConnectionString());
                con.Open();
                string sql = "Select Country, count(CustomerId) As Customers From Customer group by country order by Customers Desc";
                using SqlCommand command = new SqlCommand(sql, con);
                using SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    customers.Add(new CustomerCountry()
                    {
                        Country = reader.GetString(0), // country names
                        Customers = reader.GetInt32(1) // count of customers
                    });
                }
                return customers;
            }
            catch (Exception)
            {

                throw;
            }
        }
        /// <summary>
        /// Gets all customer names and id and total amount spent in descending order.
        /// </summary>
        /// <returns>CustomerSpender List</returns>
        public ICollection<CustomerSpender> GetHighestSpender()
        {
            try
            {
                ICollection<CustomerSpender> customers = new List<CustomerSpender>(); ;
                using SqlConnection con = new SqlConnection(GetConnectionString());
                con.Open();
                string sql = "select Customer.CustomerId, Firstname, sum(total)\r\nfrom Invoice inner join Customer on Invoice.CustomerId = Customer.CustomerId\r\ngroup by Firstname, Customer.CustomerId\r\norder by sum(total) Desc";
                using SqlCommand command = new SqlCommand(sql, con);
                using SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    customers.Add(new CustomerSpender()
                    {
                        CustomerId = reader.GetInt32(0), // customer id
                        FirstName = reader.GetString(1), // first name
                        Spent = (Double)reader.GetDecimal(2) // total spent
                    });
                }
                return customers;
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Gets the most popular genre for a spesific customer, will return multiple on ties.
        /// </summary>
        /// <param name="id">CustomerId</param>
        /// <returns>CustomerGenre List</returns>
        public ICollection<CustomerGenre> GetMostPopularGenre(int id)
        {
            ICollection<CustomerGenre> customers = new List<CustomerGenre>();
            try
            {
                using SqlConnection con = new SqlConnection(GetConnectionString());
                con.Open();
                string sql = $"select Genre.Name, count(Genre.Name)\r\nfrom InvoiceLine inner join Invoice on InvoiceLine.InvoiceId = Invoice.InvoiceId\r\ninner join Track on InvoiceLine.TrackId = Track.TrackId\r\ninner join Genre on Genre.GenreId =track.GenreId\r\nwhere invoice.CustomerId = {id}\r\ngroup by Genre.Name\r\nOrder by Count(Genre.name) Desc";
                using SqlCommand command = new SqlCommand(sql, con);
                using SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    customers.Add(new CustomerGenre()
                    {
                       genre = reader.GetString(0), // genre
                       Count= reader.GetInt32(1) // count
                    });
                }
            }
            catch (Exception)
            {

                throw;
            }
            return customers.Where(x => x.Count == customers.Select(x => x.Count).Max()).ToList();
        }
        /// <summary>
        /// Gets a subset of all customers
        /// </summary>
        /// <param name="limit">Amount of customers.</param>
        /// <param name="offset">From where the subset will begin.</param>
        /// <returns>Customer List</returns>
        public ICollection<Customer> GetSubset(int limit, int offset)
        {
            ICollection<Customer> customers = new List<Customer>(); ;
            using SqlConnection con = new SqlConnection(GetConnectionString());
            con.Open();
            string sql = $"SELECT * FROM CUSTOMER ORDER BY CustomerId OFFSET {offset} ROWS FETCH NEXT {limit} ROWS ONLY";
            using SqlCommand command = new SqlCommand(sql, con);
            using SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                customers.Add(new Customer()
                {
                    Id = reader.GetInt32(0), // id
                    FirstName = reader.GetString(1), // fname
                    LastName = reader.GetString(2), // lname
                    Country = reader.GetString(7), // country
                    PostalCode = reader.IsDBNull(8) ? null : reader.GetString(8), // postcode
                    PhoneNumber = reader.IsDBNull(9) ? null : reader.GetString(9), // phone
                    Email = reader.GetString(11)// email
                });
            }
            return customers;
        }

        public bool Update(Customer customer)
        {
            using SqlConnection con = new SqlConnection(GetConnectionString());
            con.Open();
            string sql = $"update Customer set FirstName = '{customer.FirstName}', LastName = '{customer.LastName}', Country = '{customer.Country}', PostalCode ='{customer.PostalCode}', Phone='{customer.PhoneNumber}', Email = '{customer.Email}' where CustomerId = {customer.Id}";
            bool sucsess = false;
            using (SqlCommand cmd = new SqlCommand(sql, con))
            {
                sucsess = cmd.ExecuteNonQuery() == 1 ? true : false;
            }
            return true;
        }
    }
}
