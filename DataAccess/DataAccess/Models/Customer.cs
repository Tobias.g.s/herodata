﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models
{
    public class Customer
    {
        public int Id { get; set; }
        public string FirstName { get; set; } = null!;
        public string LastName { get; set; } = null!;
        public string Country { get; set; } = null!;
        public string? PostalCode { get; set; }
        public string? PhoneNumber { get; set; }
        public string? Email { get; set; }

        public override string? ToString()
        {
            return "Id: " + this.Id +" "+ "Name: " + this.FirstName+" "+ this.LastName + " " + "Country: " + this.Country + " " + "Phone: " + this.PhoneNumber + " " + "Email: " + this.Email;
        }
    }
}
