﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models
{
    public class CustomerSpender
    {
        public int CustomerId { get; set; }
        public string FirstName { get; set; } = null!;
        public double Spent { get; set; }

        public override string? ToString()
        {
            return CustomerId +" " + FirstName + " " + Spent;
        }
    }
}
