
INSERT INTO [Power] ([Name], [Description])
VALUES ('Pointy ears', 'Looks like a cool elf');

INSERT INTO [Power] ([Name], [Description])
VALUES ('Big sword', 'Its actually quite average');

INSERT INTO [Power] ([Name], [Description])
VALUES ('Throw banana', 'Throws banana');

INSERT INTO [Power] ([Name], [Description])
VALUES ('Groundstomp', 'Stomps the ground');

INSERT INTO SuperHeroPower (SuperheroId, PowerId)
VALUES (1, 1);

INSERT INTO SuperHeroPower (SuperheroId, PowerId)
VALUES (2, 2);

INSERT INTO SuperHeroPower (SuperheroId, PowerId)
VALUES (3, 3);

INSERT INTO SuperHeroPower (SuperheroId, PowerId)
VALUES (3, 2);

INSERT INTO SuperHeroPower (SuperheroId, PowerId)
VALUES (2, 1);